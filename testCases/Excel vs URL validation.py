import pyodbc as odbccon
import datetime
import openpyxl
import win32com.client as win32
from selenium import webdriver
from selenium.webdriver import Keys
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
import openpyxl
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


import time


class LoginPage():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.maximize_window()
    driver.get("http://nycwebdev02.ymcaret.org/YRS-INTEGRATED-QA-TEST-2/Login.aspx")
    wait = WebDriverWait(driver, 50)
    element1 = wait.until(EC.element_to_be_clickable((By.ID, 'TextBoxUserID')))
    element1.click()
    # time.sleep(5)
    driver.find_element(By.ID, 'TextBoxUserID').send_keys("selvam")
    driver.find_element(By.ID, 'TextBoxPassword').send_keys("Welcome1234!")
    driver.find_element(By.ID, 'ctl00_MainContent_OKButton').click()
    driver.find_element(By.XPATH, "//a[@id='lnkPageName'][contains (text(),'Estimates')]").click()
    path = "C:\\Users\\selvam\\PycharmProjects\\TextData.xlsx"
    Masterpath = "C:\\Users\\selvam\\PycharmProjects\\UAC_TestTool_v2.6_InDev.xlsx"

    inputbook = openpyxl.load_workbook(path)
    sheet = inputbook.get_sheet_by_name('TextData')
    outputsheetfrml = inputbook.create_sheet("UACtool")
    urlvalue = inputbook.create_sheet("YCAP")
    TestResult = inputbook.create_sheet("Testresults")

    outputsheetfrml.cell(row=1, column=1).value = 'Fund Id'
    outputsheetfrml.cell(row=1, column=2).value = 'M'
    outputsheetfrml.cell(row=1, column=3).value = 'C'
    outputsheetfrml.cell(row=1, column=4).value = 'J1'
    outputsheetfrml.cell(row=1, column=5).value = 'J1P'
    outputsheetfrml.cell(row=1, column=6).value = 'J7'
    outputsheetfrml.cell(row=1, column=7).value = 'J7P'
    outputsheetfrml.cell(row=1, column=8).value = 'J5'
    outputsheetfrml.cell(row=1, column=9).value = 'J5P'
    outputsheetfrml.cell(row=1, column=10).value = 'Projected_Reserves'

    urlvalue.cell(row=1, column=1).value = 'Fund Id'
    urlvalue.cell(row=1, column=2).value = 'M'
    urlvalue.cell(row=1, column=3).value = 'C'
    urlvalue.cell(row=1, column=4).value = 'J1'
    urlvalue.cell(row=1, column=5).value = 'J1P'
    urlvalue.cell(row=1, column=6).value = 'J7'
    urlvalue.cell(row=1, column=7).value = 'J7P'
    urlvalue.cell(row=1, column=8).value = 'J5'
    urlvalue.cell(row=1, column=9).value = 'J5P'
    urlvalue.cell(row=1, column=10).value = 'Projected_Reserves'

    TestResult.cell(row=1, column=1).value = 'Fund Id'
    TestResult.cell(row=1, column=2).value = 'M'
    TestResult.cell(row=1, column=3).value = 'C'
    TestResult.cell(row=1, column=4).value = 'J1'
    TestResult.cell(row=1, column=5).value = 'J1P'
    TestResult.cell(row=1, column=6).value = 'J7'
    TestResult.cell(row=1, column=7).value = 'J7P'
    TestResult.cell(row=1, column=8).value = 'J5'
    TestResult.cell(row=1, column=9).value = 'J5P'
    TestResult.cell(row=1, column=10).value = 'Projected_Reserves'
    rows = sheet.max_row

    conn = odbccon.connect("Driver={SQL Server Native Client 11.0};"
                           "Server=YRSUP_QA;"
                           "Database=yrs;"
                           "Trusted_Connection=yes;"
                           "Mars_Connection=yes")

    url_dict = {}
    Excel_dict = {}

    for r in range(1, rows + 1):
        inputfundId = str(sheet.cell(row=r, column=1).value)
        input_dict = {}
        driver.get("http://nycwebdev02.ymcaret.org/YRS-INTEGRATED-QA-TEST-2/FindInfo.aspx?Name=Estimates")

        outputsheetfrml.cell(row=r + 1, column=1).value = sheet.cell(row=r, column=1).value
        urlvalue.cell(row=r + 1, column=1).value = sheet.cell(row=r, column=1).value
        TestResult.cell(row=r + 1, column=1).value = sheet.cell(row=r, column=1).value

        driver.find_element(By.ID, 'TextboxFundNo').send_keys(inputfundId)
        driver.find_element(By.XPATH, "//input[@value='Find']").click()
        wait = WebDriverWait(driver, 50)
        element = wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@type='image']")))
        element.click()
        wait = WebDriverWait(driver, 50)
        element2 = wait.until(EC.element_to_be_clickable((By.XPATH, "//span[@id='PopcalendarDate_Control']")))
        element2.click()
        # driver.find_element(By.XPATH, "//span[@id='PopcalendarDate_Control']").click()
        driver.find_element(By.XPATH, "//span[@id='popupSuperSpanYear0']").click()
        driver.find_element(By.XPATH, "//td[contains(text(),'2023')]").click()
        driver.find_element(By.XPATH, "//span[@id='popupSuperSpanMonth0']").click()
        driver.find_element(By.XPATH, "//td[contains(text(),'April')]").click()
        driver.find_element(By.XPATH,
                            "(//span[@id='popupSuperContent0']//table//span[contains(text(),'1')])[1]").click()
        time.sleep(5)
        wait = WebDriverWait(driver, 50)
        element2 = wait.until(EC.element_to_be_clickable((By.XPATH, "//span[@id='PopcalendarDate_Control']")))
        element2.click()
        # driver.find_element(By.XPATH, "//span[@id='PopcalendarDate_Control']").click()
        driver.find_element(By.XPATH, "//span[@id='popupSuperSpanYear0']").click()
        driver.find_element(By.XPATH, "//td[contains(text(),'2022')]").click()
        driver.find_element(By.XPATH, "//span[@id='popupSuperSpanMonth0']").click()
        driver.find_element(By.XPATH, "//td[contains(text(),'August')]").click()
        driver.find_element(By.XPATH,
                            "(//span[@id='popupSuperContent0']//table//span[contains(text(),'1')])[1]").click()

        driver.find_element(By.XPATH, "//*[@id='tabStripRetirementEstimate']/tbody/tr/td[2]/font/b").click()
        driver.find_element(By.NAME, "TextBoxEndWorkDate").send_keys("08/01/2022")
        driver.find_element(By.NAME, "ButtonUpdateEmployment").click()
        wait = WebDriverWait(driver, 50)
        element3 = wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@id='Buttoncalculate']")))
        element3.click()

        # driver.find_element(By.XPATH, "//input[@id='Buttoncalculate']").click()

        page = driver.find_element(By.TAG_NAME, ("html"))
        page.send_keys(Keys.END)
        # time.sleep(10)
        input_dict1 = {}
        driver.find_element(By.XPATH, "(//td[@class='Td_HeadingFormContainer'])[2]").click()
        Projected_Reserves = driver.find_element(By.XPATH, "//input[@name='txtProjectedReserves']").get_attribute(
            'value')
        # print(Projected_Reserves)
        wait = WebDriverWait(driver, 50)
        element4 = wait.until(
            EC.element_to_be_clickable((By.XPATH, ("//table[@id='DatatGridSocialSecurityLevel']"))))
        element4.click()
        summarytable = driver.find_element(By.XPATH, ("//table[@id='DatatGridSocialSecurityLevel']"))
        summaryrow = len(driver.find_elements(By.XPATH, ("//table[@id='DatatGridSocialSecurityLevel']//tr")))

        for i in range(1, summaryrow + 1):
            Annuity = driver.find_element(By.XPATH,
                                          ("//table[@id='DatatGridSocialSecurityLevel']//tbody//tr[" + str(
                                              i) + "]/td[1]")).text
            Retire = driver.find_element(By.XPATH, (
                    "//table[@id='DatatGridSocialSecurityLevel']//tbody//tr[" + str(i) + "]/td[2]")).text
            Survivor = driver.find_element(By.XPATH, (
                    "//table[@id='DatatGridSocialSecurityLevel']//tbody//tr[" + str(i) + "]/td[3]")).text
            Beneficiary = driver.find_element(By.XPATH, (
                    "//table[@id='DatatGridSocialSecurityLevel']//tbody//tr[" + str(i) + "]/td[4]")).text
            input_dict1[Annuity] = Retire

        # urlvalues_annuity = {}

        urlvalue.cell(row=r + 1, column=2).value = input_dict1.get('M', '')
        urlvalue.cell(row=r + 1, column=3).value = input_dict1.get('C', '')
        urlvalue.cell(row=r + 1, column=4).value = input_dict1.get('J1',0)
        urlvalue.cell(row=r + 1, column=5).value = input_dict1.get('J1P',0)
        urlvalue.cell(row=r + 1, column=6).value = input_dict1.get('J7',0)
        urlvalue.cell(row=r + 1, column=7).value = input_dict1.get('J7P',0)
        urlvalue.cell(row=r + 1, column=8).value = input_dict1.get('J5',0)
        urlvalue.cell(row=r + 1, column=9).value = input_dict1.get('J5P',0)
        urlvalue.cell(row=r + 1, column=10).value = Projected_Reserves
        url_dict[inputfundId] = {'M': urlvalue.cell(row=r + 1, column=2).value,
                                 'C': urlvalue.cell(row=r + 1, column=3).value,
                                 'J1': urlvalue.cell(row=r + 1, column=4).value,
                                 'J1P': urlvalue.cell(row=r + 1, column=5).value,
                                 'J7': urlvalue.cell(row=r + 1, column=6).value,
                                 'J7P': urlvalue.cell(row=r + 1, column=7).value,
                                 'J5': urlvalue.cell(row=r + 1, column=8).value,
                                 'J5P': urlvalue.cell(row=r + 1, column=9).value,
                                 'Projected_Reserves': urlvalue.cell(row=r + 1, column=10).value}

        # urlvalues_annuity[r]={'url':url_dict}

        # print(url_dict[inputfundId])

        # input_dict = {}
        cursor = conn.cursor()
        first_row=cursor.execute(
            "SELECT AP.dtmBirthDate, AB.dtmBirthDate from dbo.ATSPERSS AP, dbo.AtsBeneficiaries AB WHERE intFundIdNo = " + inputfundId + " AND AP.guiUniqueID = AB.guiPersID AND AB.chvBeneficiaryGroupCode = 'PRIM' AND AB.chvBeneficiaryTypeCode = 'MEMBER'").fetchone()
        AnnuitantDOB = first_row[0].strftime("%m/%d/%Y")
        input_dict['AnnuitantDOB'] = AnnuitantDOB
        if first_row[1] != None:
            BeneficiaryDOB = first_row[1].strftime("%m/%d/%Y")
            input_dict['BeneficiaryDOB'] = BeneficiaryDOB


        temp2 = {}
        temp2_count = 1
        cursor.execute(
            "SELECT DISTINCT TOP 2  AT.dtsTransactDate FROM   dbo.AtsPerss AP,  dbo.AtsTransacts AT Where ap.intFundIdNo = " + inputfundId + " and AP.guiUniqueID = AT.guiPersID and AT.chrTransactType = 'MCPR' order by AT.dtsTransactDate desc")
        for row2 in cursor:
            LastfundedContributiondates = row2[0].strftime("%m/%d/%Y")
            LastfundedContributiondates = datetime.datetime.strptime(str(LastfundedContributiondates),
                                                                     "%m/%d/%Y").date()

            temp2[temp2_count] = LastfundedContributiondates
            temp2_count = temp2_count + 1
        input_dict['LastfundedContributiondates'] = temp2

        cursor.execute(
            "SELECT ayr.numYmcaPctg as '401a - YMCA %', ayr.numConstituentPctg as '401a - Part %' FROM dbo.AtsYmcaRess ayr, dbo.AtsYmcas ay, AtsEmpEvents ae, dbo.ATSPERSS ap where ayr.guiYmcaID = ay.guiUniqueID and ap.guiUniqueID = ae.guiPersId and ae.chrStatusType = 'A' and ap.intFundIdNo = " + inputfundId + " and ae.guiYmcaId = ay.guiUniqueID and dtmTermDate IS NULL")
        for row3 in cursor:
            YMCA_401A = row3[0]
            # YMCA_401A= print("{:.0%}".format(YMCA_401A))
            input_dict['YMCA_401A'] = float(YMCA_401A) / 100

            PART_401A = row3[1]
            # PART_401A = print("{:.0%}".format(PART_401A))
            input_dict['PART_401A'] = float(PART_401A) / 100

        cursor.execute(
            "SELECT aee.numAddlPctg as '403b %', aee.mnyAddlContribution as '403b $' FROM dbo.ATSPERSS ap, dbo.AtsEmpEvents ae, dbo.atsempelectives aee where ap.intFundIdNo = " + inputfundId + " and ap.guiUniqueID = ae.guiPersId and ae.guiUniqueID = aee.guiEmpEventID and ae.chrStatusType = 'A' and aee.dtsTerminationDate is null")
        for row4 in cursor:

            try:
                YMCA_403bper = row4[0]
                input_dict['403b%'] = float(YMCA_403bper) / 100

            except KeyError:
                print(inputfundId + "has no value in 403b%")
            try:
                YMCA_403bdollar = row4[1]
                input_dict['403b$'] = float(YMCA_403bdollar)

            except KeyError:
                print(inputfundId + "has no value in 403b$")

        cursor.execute(
            "Select distinct yp.NumPayPeriod FROM dbo.AtsYmcaRess ayr, dbo.AtsYmcas ay, AtsEmpEvents ae, dbo.ATSPERSS ap, dbo.YmcaPayPeriod yp where ayr.guiYmcaID = ay.guiUniqueID and ap.guiUniqueID = ae.guiPersId and yp.GuiYmcaid = ayr.guiYmcaID and  ae.guiYmcaId = ay.guiUniqueID and ae.chrStatusType = 'A' and ap.intFundIdNo = " + inputfundId)
        for row5 in cursor:
            Payroll_freq = row5[0]
            if Payroll_freq == 26:
                input_dict['Payroll_freq'] = "Bi-Weekly"
            elif Payroll_freq == 24:
                input_dict['Payroll_freq'] = "Semi-Monthly"
            elif Payroll_freq == 12:
                input_dict['Payroll_freq'] = "Monthly"

        cursor.execute(
            "DECLARE @FundNo int = " + inputfundId + " DECLARE @TermDate DATE = '2022-01-01' DECLARE @Persid UNIQUEIDENTIFIER DECLARE @RCOUNT INT SELECT @Persid = guiUniqueID FROM AtsPerss (NOLOCK) WHERE intFundIdNo = @FundNo SELECT @RCOUNT =COUNT(A.AA) FROM (SELECT COUNT(dtsTransactDate) AA FROM AtsTransacts (NOLOCK) WHERE guiPersID = @Persid AND chrTransactType IN ('MCPR','UPPR','OPPR') AND chrAcctType IN (SELECT chrAcctType FROM AtsMetaAcctTypes (NOLOCK) WHERE bitAcctIncludeForRetirement = 1 AND bitBasicAcct =1) AND dtsTransactDate > = DATEADD(YY,-1,@TermDate) AND dtsTransactDate < = @TermDate AND mnyMonthlyComp <> 0 GROUP BY guiPersID,YEAR(dtsTransactDate), MONTH(dtsTransactDate)) AS A SELECT SUM(mnyMonthlyComp)/@RCOUNT AverageSalary FROM AtsTransacts (NOLOCK) WHERE guiPersID = @Persid AND chrTransactType IN ('MCPR','UPPR','OPPR') AND chrAcctType IN (SELECT chrAcctType FROM AtsMetaAcctTypes WHERE bitAcctIncludeForRetirement = 1 AND bitBasicAcct =1) AND dtsTransactDate > = DATEADD(YY,-1,@TermDate) AND dtsTransactDate < = @TermDate")
        for row7 in cursor:
            Avg_monsalary = row7[0]
            input_dict['Avg'] = Avg_monsalary

        cursor.execute(
            "DECLARE @FundNo int = " + inputfundId + " DECLARE @Persid UNIQUEIDENTIFIER DECLARE @RCOUNT INT SELECT @Persid = guiUniqueID FROM AtsPerss (NOLOCK) WHERE intFundIdNo = @FundNo SELECT guipersid, intNonPaid AS ServiceMonths, CAST(intNonPaid AS smallmoney) / 12 AS ServiceYears FROM dbo.AtsFundEvents WHERE guiPersid = @Persid")
        for row9 in cursor:
            Serv_Year = row9[2]
            input_dict['Seryear'] = Serv_Year

        temp3 = {'fundid': [], 'accType': [], 'annBseType': [], 'perPreTax': [], 'perPosTax': [],
                 'ymcPreTax': [], 'Funded': []}
        startingrow = 1

        cursor.execute(
            "SELECT dbo.AtsPerss.intFundIdNo, dbo.AtsTransacts.chrAcctType, dbo.AtsTransacts.chrAnnuityBasisType, SUM(dbo.AtsTransacts.mnyPersonalPreTax) AS PersPreTax, SUM(dbo.AtsTransacts.mnyPersonalPostTax) AS PersPostTax, SUM(dbo.AtsTransacts.mnyYmcaPreTax) AS YmcaPreTax, '' as Funded FROM dbo.AtsPerss INNER JOIN dbo.AtsTransacts ON dbo.AtsPerss.guiUniqueID = dbo.AtsTransacts.guiPersID where dbo.AtsTransacts.dtsFundedDate is not null GROUP BY dbo.AtsPerss.intFundIdNo, dbo.AtsTransacts.chrAcctType, dbo.AtsTransacts.chrAnnuityBasisType HAVING (dbo.AtsPerss.intFundIdNo = " + inputfundId + ") UNION SELECT dbo.AtsPerss.intFundIdNo, dbo.AtsTransacts.chrAcctType, dbo.AtsTransacts.chrAnnuityBasisType, SUM(dbo.AtsTransacts.mnyPersonalPreTax) AS PersPreTax, SUM(dbo.AtsTransacts.mnyPersonalPostTax) AS PersPostTax, SUM(dbo.AtsTransacts.mnyYmcaPreTax) AS YmcaPreTax, 'N' as Funded FROM dbo.AtsPerss INNER JOIN dbo.AtsTransacts ON dbo.AtsPerss.guiUniqueID = dbo.AtsTransacts.guiPersID where dbo.AtsTransacts.dtsFundedDate is null GROUP BY dbo.AtsPerss.intFundIdNo, dbo.AtsTransacts.chrAcctType, dbo.AtsTransacts.chrAnnuityBasisType HAVING (dbo.AtsPerss.intFundIdNo = " + inputfundId + ") ORDER BY 2, 3")
        for row6 in cursor:
            temp3['fundid'].append(row6[0])
            temp3['accType'].append(row6[1])
            temp3['annBseType'].append(row6[2])
            temp3['perPreTax'].append(row6[3])
            temp3['perPosTax'].append(row6[4])
            temp3['ymcPreTax'].append(row6[5])
            temp3['Funded'].append(row6[6])

        input_dict['AccountInfo'] = temp3

        # Fundid = row6
        # startingrow += 1
        # startingcol = 1
        # for c in cursor:
        #   startingcol += 1

        workbook2 = openpyxl.load_workbook(Masterpath)

        Mastersheet = workbook2.get_sheet_by_name('Main')
        Mastersheet.cell(row=16, column=3).value = input_dict['AnnuitantDOB']
        try:
            Mastersheet.cell(row=17, column=3).value = input_dict['BeneficiaryDOB']
        except KeyError:
            Mastersheet.cell(row=17, column=6).value = ''
        Mastersheet.cell(row=18, column=3).value = input_dict['LastfundedContributiondates'][1]
        Mastersheet.cell(row=19, column=3).value = input_dict['LastfundedContributiondates'][2]
        Mastersheet.cell(row=13, column=6).value = input_dict['Avg']
        Mastersheet.cell(row=15, column=6).value = input_dict['YMCA_401A']
        Mastersheet.cell(row=16, column=6).value = input_dict['PART_401A']
        Mastersheet.cell(row=20, column=6).value = input_dict['Seryear']
        try:
            Mastersheet.cell(row=17, column=6).value = input_dict['403b%']
        except KeyError:
            Mastersheet.cell(row=17, column=6).value = '0%'
        try:
            Mastersheet.cell(row=17, column=7).value = input_dict['403b$']
        except KeyError:
            Mastersheet.cell(row=17, column=7).value = '0'
        Mastersheet.cell(row=18, column=6).value = input_dict['Payroll_freq']

        noofRows = len(input_dict.get('AccountInfo').get('fundid'))
        initialRow = 29
        for printrow in range(0, noofRows):
            Mastersheet.cell(row=initialRow, column=2).value = input_dict.get('AccountInfo').get('fundid')[
                printrow]
            Mastersheet.cell(row=initialRow, column=3).value = input_dict.get('AccountInfo').get('accType')[
                printrow]
            Mastersheet.cell(row=initialRow, column=4).value = input_dict.get('AccountInfo').get('annBseType')[
                printrow]
            Mastersheet.cell(row=initialRow, column=5).value = input_dict.get('AccountInfo').get('perPreTax')[
                printrow]
            Mastersheet.cell(row=initialRow, column=6).value = input_dict.get('AccountInfo').get('perPosTax')[
                printrow]
            Mastersheet.cell(row=initialRow, column=7).value = input_dict.get('AccountInfo').get('ymcPreTax')[
                printrow]
            Mastersheet.cell(row=initialRow, column=9).value = input_dict.get('AccountInfo').get('Funded')[
                printrow]
            initialRow = initialRow + 1

        workbook2.save("C:\\Users\\selvam\\PycharmProjects\\UAC_TestTool_Demo.xlsx")
        Masterpath2 = "C:\\Users\\selvam\\PycharmProjects\\UAC_TestTool_Demo.xlsx"

        excel = win32.gencache.EnsureDispatch('Excel.Application')
        workbook = excel.Workbooks.Open('C:\\Users\\selvam\\PycharmProjects\\UAC_TestTool_Demo.xlsx')
        # this must be the absolute path (r'C:/abc/def/ghi')
        workbook.Save()
        workbook.Close()
        excel.Quit()
        workbook3 = openpyxl.load_workbook(Masterpath2, data_only=True)
        Mastersheet2 = workbook3.get_sheet_by_name('Main')

        outputsheetfrml.cell(row=r + 1, column=2).value = Mastersheet2['O12'].internal_value
        outputsheetfrml.cell(row=r + 1, column=3).value = Mastersheet2['O13'].internal_value
        outputsheetfrml.cell(row=r + 1, column=4).value = Mastersheet2['O14'].internal_value
        outputsheetfrml.cell(row=r + 1, column=5).value = Mastersheet2['O15'].internal_value
        outputsheetfrml.cell(row=r + 1, column=6).value = Mastersheet2['O16'].internal_value
        outputsheetfrml.cell(row=r + 1, column=7).value = Mastersheet2['O17'].internal_value
        outputsheetfrml.cell(row=r + 1, column=8).value = Mastersheet2['O18'].internal_value
        outputsheetfrml.cell(row=r + 1, column=9).value = Mastersheet2['O19'].internal_value
        outputsheetfrml.cell(row=r + 1, column=10).value = str(round(Mastersheet2['L22'].internal_value, 2))
        Excel_dict[inputfundId] = {'M': outputsheetfrml.cell(row=r + 1, column=2).value,
                                   'C': outputsheetfrml.cell(row=r + 1, column=3).value,
                                   'J1': outputsheetfrml.cell(row=r + 1, column=4).value,
                                   'J1P': outputsheetfrml.cell(row=r + 1, column=5).value,
                                   'J7': outputsheetfrml.cell(row=r + 1, column=6).value,
                                   'J7P': outputsheetfrml.cell(row=r + 1, column=7).value,
                                   'J5': outputsheetfrml.cell(row=r + 1, column=8).value,
                                   'J5P': outputsheetfrml.cell(row=r + 1, column=9).value,
                                   'Projected_Reserves': outputsheetfrml.cell(row=r + 1, column=10).value}
        print(inputfundId + ':')
        print(Excel_dict[inputfundId])
        print(url_dict[inputfundId])

        output_row = 1
        col_map = {'M': 2, 'C': 3, 'J1': 4, 'J1P': 5, 'J7': 6, 'J7P': 7, 'J5': 8, 'J5P': 9, 'Projected_Reserves': 10}

        for fund_id in url_dict.keys():
            output_row = output_row + 1
            TestResult.cell(row=output_row, column=1).value = fund_id
            for Annuities in url_dict.get(fund_id).keys():
                if url_dict.get(fund_id).get(Annuities) != '' or url_dict.get(fund_id).get(Annuities) != str(
                        '*N/A'):
                    if url_dict.get(fund_id).get(Annuities) != Excel_dict.get(fund_id).get(Annuities):
                        diff = float(abs(float(url_dict.get(fund_id).get(Annuities)) - float(
                            Excel_dict.get(fund_id).get(Annuities))))
                        if diff > 1.5:
                            TestResult.cell(row=output_row,
                                            column=col_map.get(
                                                Annuities)).value = 'Not Matching: diff value ' + str(
                                round(diff, 2))
                        else:
                            TestResult.cell(row=output_row, column=col_map.get(Annuities)).value = 'Matching'
                    else:
                        TestResult.cell(row=output_row, column=col_map.get(Annuities)).value = 'Matching'

        inputbook.save("C:\\Users\\selvam\\PycharmProjects\\TextData.xlsx")

