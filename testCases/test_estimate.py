import allure
import pytest
import time

from selenium.webdriver.common.by import By

from pageObjects.LoginPage import LoginPage
from pageObjects.Estimate import Estimate
from utilities.readProperties import ReadConfig
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from array import *
import allure
import csv





@allure.severity(allure.severity_level.NORMAL)
class Test_002_estimate:
    baseURL = ReadConfig.getApplicationURL()
    baseURL2 = ReadConfig.getApplicationURL2()
    baseurlist = {"url1":baseURL,"url2":baseURL2}
    username = ReadConfig.getUsername()
    password = ReadConfig.getPassword()



    @allure.severity(allure.severity_level.MINOR)


    def test_estimate(self, setup):
        with open('C:\\Users\\selvam\\PycharmProjects\\TextData.csv') as csvfile:
            fundlist = csv.reader(csvfile, delimiter=',')
            for fund_num in (fundlist):
                url1dict_Retirement = {}
                url1dict_Saving = {}

                for url_name in (self.baseURL, self.baseURL2):
                    self.driver = setup
                    self.driver.get(url_name)
                    self.driver.maximize_window()
                    self.lp = LoginPage(self.driver)
                    self.lp.set_user_name(self.username)

                    wait = WebDriverWait(self.driver, 30)
                    wait.until(
                        EC.visibility_of_element_located((By.CSS_SELECTOR, 'input[name="ctl00$MainContent$TextBoxPassword"]')))
                    self.lp.set_password(self.password)
                    self.lp.click_login()


                    self.estimate = Estimate(self.driver)
                    self.estimate.clickOnRetestlink()
                    time.sleep(5)
                    self.estimate.setFundnum(fund_num)
                    self.estimate.clickFind()
                    wait= WebDriverWait(self.driver,50)
                    element=wait.until(EC.element_to_be_clickable((By.XPATH,"//input[@type='image']")))
                    element.click()
                    self.driver.save_screenshot(".\\Screenshots\\" + "estimate page.png")
                    self.estimate.clickdate()
                    self.estimate.clickyear()
                    self.estimate.clickyrdrp()
                    self.estimate.clickmonthdrp()
                    self.estimate.clickmonth()
                    self.estimate.clickdatesel()
                    time.sleep(5)

                    self.estimate.clickcalculate()
                    time.sleep(10)
                    page = self.driver.find_element_by_tag_name("html")
                    page.send_keys(Keys.END)
                    time.sleep(10)
                    self.driver.save_screenshot(".\\Screenshots\\" + "summary.png")

                    summarytable= self.driver.find_element_by_xpath("//table[@id='DatatGridSocialSecurityLevel']")
                    summaryrow= len(self.driver.find_elements_by_xpath("//table[@id='DatatGridSocialSecurityLevel']//tr"))


                    for i in range(2, summaryrow + 1):
                        Annuity= self.driver.find_element_by_xpath(
                                    "//table[@id='DatatGridSocialSecurityLevel']//tbody//tr["+str(i)+"]/td[1]").text
                        Retire= self.driver.find_element_by_xpath(
                                    "//table[@id='DatatGridSocialSecurityLevel']//tbody//tr["+str(i)+"]/td[2]").text
                        Survivor= self.driver.find_element_by_xpath(
                                    "//table[@id='DatatGridSocialSecurityLevel']//tbody//tr["+str(i)+"]/td[3]").text
                        Beneficiary= self.driver.find_element_by_xpath(
                                    "//table[@id='DatatGridSocialSecurityLevel']//tbody//tr["+str(i)+"]/td[4]").text

                    SSbenefit = self.driver.find_element_by_xpath("//input[@id='TextboxFromBenefitValue']").text
                    print(SSbenefit)
                    SSincrease = self.driver.find_element_by_xpath("//input[@id='TextboxSSIncrease']").text
                    print(SSincrease)
                    Projectedreserves = self.driver.find_element_by_xpath("//input[@id='txtProjectedReserves']").text
                    print(Projectedreserves)
                    deathbenefitused = self.driver.find_element_by_xpath("//input[@id='txtDeathBenefitUsed']").text
                    print(deathbenefitused)
                    projectedfinalyearsalary = self.driver.find_element_by_xpath(
                        "//input[@id='TextBoxProjFinalYrsSalary']").text
                    print(projectedfinalyearsalary)



                    self.estimate.clickemployment()

                    page = self.driver.find_element_by_tag_name("html")
                    page.send_keys(Keys.END)
                    time.sleep(10)
                    self.driver.save_screenshot(".\\Screenshots\\" + "employment.png")
                    self.estimate.clickretriement()

                    page = self.driver.find_element_by_tag_name("html")
                    page.send_keys(Keys.END)
                    time.sleep(10)
                    self.driver.save_screenshot(".\\Screenshots\\" + "retirement.png")

                    self.estimate.clickaccounts()

                    page = self.driver.find_element_by_tag_name("html")
                    page.send_keys(Keys.END)
                    time.sleep(10)
                    self.driver.save_screenshot(".\\Screenshots\\" + "accounts.png")

                    Plantype = Select(self.driver.find_element_by_xpath("//select[@name='DropDownListPlanType']"))
                    option = Plantype.first_selected_option
                    e = option.text


                    if e == "Retirement" or e == "Both":
                        table = self.driver.find_element_by_xpath("//table[@id='DatagridElectiveRetirementAccounts']")
                        row = len(self.driver.find_elements_by_xpath("//table[@id='DatagridElectiveRetirementAccounts']//tr"))
                        column = len(self.driver.find_elements_by_xpath("//table[@id='DatagridElectiveRetirementAccounts']//tr[2]//td"))

                        ret_dict = {}

                        for i in range(2, row + 1):
                                RetAccountname= self.driver.find_element_by_xpath(
                                    "//table[@id='DatagridElectiveRetirementAccounts']//tr["+str(i)+"]//td[2]").text
                                RetAccounttotal = self.driver.find_element_by_xpath(
                                    "//table[@id='DatagridElectiveRetirementAccounts']//tr["+str(i)+"]//td[3]").text
                                RetProjectedbalance = self.driver.find_element_by_xpath(
                                    "//table[@id='DatagridElectiveRetirementAccounts']//tr["+str(i)+"]//td[4]").text


                                ret_dict[RetAccountname] = {'RetAccounttotal':RetAccounttotal, 'RetProjectedbalance':RetProjectedbalance}
                        url1dict_Retirement[url_name]= {'Retirement':ret_dict}
                    if e == "Savings" or e == "Both":
                        savingstable= self.driver.find_element_by_xpath("//table[@id='DatagridElectiveSavingsAccounts']")
                        savingsrow= len(self.driver.find_elements_by_xpath("//table[@id='DatagridElectiveSavingsAccounts']//tr"))

                        sav_dict = {}
                        for i in range(2, savingsrow + 1):
                            savAccountname= self.driver.find_element_by_xpath(
                                    "//table[@id='DatagridElectiveSavingsAccounts']//tr["+str(i)+"]//td[2]").text
                            savAccounttotal= self.driver.find_element_by_xpath(
                                    "//table[@id='DatagridElectiveSavingsAccounts']//tr["+str(i)+"]//td[3]").text
                            savProjectedbalance= self.driver.find_element_by_xpath(
                                    "//table[@id='DatagridElectiveSavingsAccounts']//tr["+str(i)+"]//td[4]").text

                            sav_dict[savAccountname] = {'savAccounttotal': savAccounttotal,
                                                            'savProjectedbalance': savProjectedbalance}
                        url1dict_Saving[url_name]={'Savings':sav_dict}
                    else:
                        print("no table exists")


                for Account_name in url1dict_Retirement.get(self.baseURL).get('Retirement').keys():
                    for Accountcol in url1dict_Retirement.get(self.baseURL).get('Retirement').get(Account_name).keys():
                        if url1dict_Retirement.get(self.baseURL2).get('Retirement').get(Account_name) != None:
                            if url1dict_Retirement.get(self.baseURL).get('Retirement').get(Account_name).get(Accountcol)!= url1dict_Retirement.get(self.baseURL2).get('Retirement').get(Account_name).get(Accountcol):
                                print(fund_num,'YRS Legacy-Retirement:', Account_name, Accountcol, url1dict_Retirement.get(self.baseURL).get('Retirement').get(Account_name).get(Accountcol))
                                print(fund_num, 'YCAP-Retirement:', Account_name, Accountcol,
                                      url1dict_Retirement.get(self.baseURL2).get('Retirement').get(Account_name).get(
                                          Accountcol))

                for Account_name in url1dict_Saving.get(self.baseURL).get('Savings').keys():
                    for Accountcol in url1dict_Saving.get(self.baseURL).get('Savings').get(Account_name).keys():
                        if url1dict_Saving.get(self.baseURL2).get('Savings').get(Account_name)!= None:
                            if url1dict_Saving.get(self.baseURL).get('Savings').get(Account_name).get(Accountcol)!= url1dict_Saving.get(self.baseURL2).get('Savings').get(Account_name).get(Accountcol):
                                print(fund_num, 'YRS Legacy-Savings:', Account_name, Accountcol,
                                      url1dict_Saving.get(self.baseURL).get('Savings').get(Account_name).get(
                                          Accountcol))
                                print(fund_num, 'YCAP-Savings:', Account_name, Accountcol,
                                      url1dict_Saving.get(self.baseURL2).get('Savings').get(Account_name).get(
                                          Accountcol))