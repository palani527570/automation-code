from selenium import webdriver
import pytest
from webdriver_manager.chrome import ChromeDriverManager
from pageObjects.LoginPage import LoginPage
from utilities.readProperties import ReadConfig



baseURL = ReadConfig.getApplicationURL()
username = ReadConfig.getUsername()
password = ReadConfig.getPassword()

def test_login(setup):

    driver = setup
    driver.get(baseURL)
    lp = LoginPage(driver)
    lp.set_user_name(username)
    lp.set_password(password)
    lp.click_login()
    driver.implicitly_wait(5)
    act_title = driver.title
    if act_title == "YMCA YRS":
        assert True
        driver.save_screenshot(".\\Screenshots\\" + "test_login_passed.png")

        driver.close()
    else:
        driver.save_screenshot(".\\Screenshots\\" + "test_login_failed.png")
        driver.close()

        assert False