import requests

resp= requests.get('https://reqres.in/api/users?page=2', verify=False)
print(resp)
#print(type(resp))
#print(dir(resp))
#print(resp.text)
print(resp.json())
#print(resp.content)
print(resp.headers)
print(resp.cookies)
print(resp.encoding)
print(resp.url)