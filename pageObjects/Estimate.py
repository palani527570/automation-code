import time
from selenium.webdriver.support.ui import Select

class Estimate:
    lnk_retirement_estimate_xpath = "//a[@id='lnkPageName'][contains (text(),'Estimates')]"
    textbox_fundnum_id = "TextboxFundNo"
    btn_find_xpath = "//input[@value='Find']"
    btn_radio_xpath = "//input[@type='image']"
    btn_calculate_xpath = "//input[@id='Buttoncalculate']"
    txt_employment_xpath = "//*[@id='tabStripRetirementEstimate']/tbody/tr/td[2]/font/b"
    txt_retirement_xpath = "//*[@id='tabStripRetirementEstimate']/tbody/tr/td[3]/font/b"
    txt_accounts_xpath = "//*[@id='tabStripRetirementEstimate']/tbody/tr/td[4]/font/b"
    btn_ssn_xpath = "(//input[@type='image'])[1]"
    btn_dep_xpath = "(//input[@type='image'])[2]"
    date_xpath = "//span[@id='PopcalendarDate_Control']"
    year_xpath = "//span[@id='popupSuperSpanYear0']"
    datesel_xpath = "(//span[@id='popupSuperContent0']//table//span[contains(text(),'1')])[1]"
    monthdrp_xpath = "//span[@id='popupSuperSpanMonth0']"
    month_xpath = "//td[contains(text(),'March')]"
    drpyr_xpath = "//td[contains(text(),'2023')]"



    def __init__(self,driver):
        self.driver=driver

    def clickOnRetestlink(self):
        self.driver.find_element_by_xpath(self.lnk_retirement_estimate_xpath).click()

    def setFundnum(self, fundnum):
        self.driver.find_element_by_id(self.textbox_fundnum_id).send_keys(fundnum)

    def clickFind(self):
        self.driver.find_element_by_xpath(self.btn_find_xpath).click()

    def clickradio(self):
        self.driver.find_element_by_xpath(self.btn_radio_xpath).click()

    def clickdate(self):
        self.driver.find_element_by_xpath(self.date_xpath).click()

    def clickyear(self):
        self.driver.find_element_by_xpath(self.year_xpath).click()

    def clickyrdrp(self):
        self.driver.find_element_by_xpath(self.drpyr_xpath).click()

    def clickmonthdrp(self):
        self.driver.find_element_by_xpath(self.monthdrp_xpath).click()

    def clickmonth(self):
        self.driver.find_element_by_xpath(self.month_xpath).click()

    def clickdatesel(self):
        self.driver.find_element_by_xpath(self.datesel_xpath).click()

    def clickcalculate(self):
        self.driver.find_element_by_xpath(self.btn_calculate_xpath).click()

    def clickemployment(self):
        self.driver.find_element_by_xpath(self.txt_employment_xpath).click()

    def clickretriement(self):
        self.driver.find_element_by_xpath(self.txt_retirement_xpath).click()

    def clickaccounts(self):
        self.driver.find_element_by_xpath(self.txt_accounts_xpath).click()

    def clicksubssn(self):
        self.driver.find_element_by_xpath(self.btn_ssn_xpath).click()

    def clickdepssn(self):
        self.driver.find_element_by_xpath(self.btn_dep_xpath).click()











