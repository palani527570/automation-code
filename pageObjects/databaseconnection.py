import pyodbc as odbccon
import datetime
import openpyxl
import win32com.client as win32
import xlsxwriter

path = "C:\\Users\\selvam\\PycharmProjects\\TextData.xlsx"
Masterpath = "C:\\Users\\selvam\\PycharmProjects\\UAC_TestTool_v2.3_InDev_52119.xlsx"

inputbook = openpyxl.load_workbook(path)
#xlsxwriterbook = xlsxwriter.Workbook(Masterpath)
sheet = inputbook.get_sheet_by_name('TextData')
outputsheetfrml = inputbook.create_sheet("ExcelCalc")

outputsheetfrml.cell(row=1, column=1).value = 'Fund Id'
outputsheetfrml.cell(row=1, column=2).value = 'M'
outputsheetfrml.cell(row=1, column=3).value = 'C'
outputsheetfrml.cell(row=1, column=4).value = 'J1'
outputsheetfrml.cell(row=1, column=5).value = 'J1P'
outputsheetfrml.cell(row=1, column=6).value = 'J7'
outputsheetfrml.cell(row=1, column=7).value = 'J7P'
outputsheetfrml.cell(row=1, column=8).value = 'J5'
outputsheetfrml.cell(row=1, column=9).value = 'J5P'

rows = sheet.max_row

conn = odbccon.connect("Driver={SQL Server Native Client 11.0};"
                       "Server=YRSUP_QA;"
                       "Database=yrs;"
                       "Trusted_Connection=yes;")
for r in range(1, rows + 1):
    inputfundId = str(sheet.cell(row=r, column=1).value)
    outputsheetfrml.cell(row=r + 1, column=1).value = sheet.cell(row=r, column=1).value
    input_dict = {}
    cursor = conn.cursor()
    cursor.execute(
        "SELECT AP.dtmBirthDate, AB.dtmBirthDate from dbo.ATSPERSS AP, dbo.AtsBeneficiaries AB WHERE intFundIdNo = " + inputfundId + " AND AP.guiUniqueID = AB.guiPersID AND AB.chvBeneficiaryGroupCode = 'PRIM' AND AB.chvBeneficiaryTypeCode = 'MEMBER'")
    for row in cursor:
        # DOB = datetime.datetime.strptime(row[0], "%Y-%m-%d").strftime("%m/%d/%Y")
        AnnuitantDOB = row[0].strftime("%m/%d/%Y")
        input_dict['AnnuitantDOB'] = AnnuitantDOB
        BeneficiaryDOB = row[1].strftime("%m/%d/%Y")
        input_dict['BeneficiaryDOB'] = BeneficiaryDOB

    temp2 = {}
    temp2_count = 1
    cursor.execute(
        "SELECT DISTINCT TOP 2  AT.dtsTransactDate FROM   dbo.AtsPerss AP,  dbo.AtsTransacts AT Where ap.intFundIdNo = " + inputfundId + " and AP.guiUniqueID = AT.guiPersID and AT.chrTransactType = 'MCPR' order by AT.dtsTransactDate desc")
    for row2 in cursor:

        LastfundedContributiondates = row2[0].strftime("%m/%d/%Y")
        LastfundedContributiondates = datetime.datetime.strptime(str(LastfundedContributiondates),"%m/%d/%Y").date()
        temp2[temp2_count] = LastfundedContributiondates
        temp2_count = temp2_count + 1
    input_dict['LastfundedContributiondates'] = temp2


    cursor.execute(
        "SELECT ayr.numYmcaPctg as '401a - YMCA %', ayr.numConstituentPctg as '401a - Part %' FROM dbo.AtsYmcaRess ayr, dbo.AtsYmcas ay, AtsEmpEvents ae, dbo.ATSPERSS ap where ayr.guiYmcaID = ay.guiUniqueID and ap.guiUniqueID = ae.guiPersId and ae.chrStatusType = 'A' and ap.intFundIdNo = " + inputfundId + " and ae.guiYmcaId = ay.guiUniqueID and dtmTermDate IS NULL")
    for row3 in cursor:
        YMCA_401A = row3[0]
        #YMCA_401A= print("{:.0%}".format(YMCA_401A))
        input_dict['YMCA_401A'] = float(YMCA_401A)/100

        PART_401A = row3[1]
        #PART_401A = print("{:.0%}".format(PART_401A))
        input_dict['PART_401A'] = float(PART_401A)/100

    cursor.execute(
        "SELECT aee.numAddlPctg as '403b %', aee.mnyAddlContribution as '403b $' FROM dbo.ATSPERSS ap, dbo.AtsEmpEvents ae, dbo.atsempelectives aee where ap.intFundIdNo = " + inputfundId + " and ap.guiUniqueID = ae.guiPersId and ae.guiUniqueID = aee.guiEmpEventID and ae.chrStatusType = 'A' and aee.dtsTerminationDate is null")
    for row4 in cursor:
        YMCA_403bper = row4[0]
        input_dict['403b%'] = float(YMCA_403bper)/100
        YMCA_403bdollar = row4[1]
        input_dict['403b$'] = float(YMCA_403bdollar)/100

    cursor.execute(
        "Select distinct yp.NumPayPeriod FROM dbo.AtsYmcaRess ayr, dbo.AtsYmcas ay, AtsEmpEvents ae, dbo.ATSPERSS ap, dbo.YmcaPayPeriod yp where ayr.guiYmcaID = ay.guiUniqueID and ap.guiUniqueID = ae.guiPersId and yp.GuiYmcaid = ayr.guiYmcaID and  ae.guiYmcaId = ay.guiUniqueID and ae.chrStatusType = 'A' and ap.intFundIdNo = " + inputfundId)
    for row5 in cursor:
        Payroll_freq = row5[0]
        if Payroll_freq == 26:
            input_dict['Payroll_freq'] = "Bi-Weekly"
        elif Payroll_freq == 24:
            input_dict['Payroll_freq'] = "Semi-Monthly"
        elif Payroll_freq == 12:
            input_dict['Payroll_freq'] = "Monthly"

    cursor.execute(
        "DECLARE @FundNo int = " + inputfundId + " DECLARE @TermDate DATE = '2021-12-01' DECLARE @Persid UNIQUEIDENTIFIER DECLARE @RCOUNT INT SELECT @Persid = guiUniqueID FROM AtsPerss (NOLOCK) WHERE intFundIdNo = @FundNo SELECT @RCOUNT =COUNT(A.AA) FROM (SELECT COUNT(dtsTransactDate) AA FROM AtsTransacts (NOLOCK) WHERE guiPersID = @Persid AND chrTransactType IN ('MCPR','UPPR','OPPR') AND chrAcctType IN (SELECT chrAcctType FROM AtsMetaAcctTypes (NOLOCK) WHERE bitAcctIncludeForRetirement = 1 AND bitBasicAcct =1) AND dtsTransactDate > = DATEADD(YY,-1,@TermDate) AND dtsTransactDate < = @TermDate AND mnyMonthlyComp <> 0 GROUP BY guiPersID,YEAR(dtsTransactDate), MONTH(dtsTransactDate)) AS A SELECT SUM(mnyMonthlyComp)/@RCOUNT AverageSalary FROM AtsTransacts (NOLOCK) WHERE guiPersID = @Persid AND chrTransactType IN ('MCPR','UPPR','OPPR') AND chrAcctType IN (SELECT chrAcctType FROM AtsMetaAcctTypes WHERE bitAcctIncludeForRetirement = 1 AND bitBasicAcct =1) AND dtsTransactDate > = DATEADD(YY,-1,@TermDate) AND dtsTransactDate < = @TermDate")
    for row7 in cursor:
        Avg_monsalary= row7[0]
        input_dict['Avg'] = Avg_monsalary

    temp3 = {'fundid': [], 'accType': [], 'annBseType': [], 'perPreTax': [], 'perPosTax': [], 'ymcPreTax': []}
    startingrow = 1

    cursor.execute(
        "SELECT dbo.AtsPerss.intFundIdNo, dbo.AtsTransacts.chrAcctType, dbo.AtsTransacts.chrAnnuityBasisType, SUM(dbo.AtsTransacts.mnyPersonalPreTax) AS PersPreTax, SUM(dbo.AtsTransacts.mnyPersonalPostTax) AS PersPostTax, SUM(dbo.AtsTransacts.mnyYmcaPreTax) AS YmcaPreTax FROM dbo.AtsPerss INNER JOIN dbo.AtsTransacts ON dbo.AtsPerss.guiUniqueID = dbo.AtsTransacts.guiPersID GROUP BY dbo.AtsPerss.intFundIdNo, dbo.AtsTransacts.chrAcctType, dbo.AtsTransacts.chrAnnuityBasisType HAVING      (dbo.AtsPerss.intFundIdNo = " + inputfundId + ")")
    for row6 in cursor:
        temp3['fundid'].append(row6[0])
        temp3['accType'].append(row6[1])
        temp3['annBseType'].append(row6[2])
        temp3['perPreTax'].append(row6[3])
        temp3['perPosTax'].append(row6[4])
        temp3['ymcPreTax'].append(row6[5])

    input_dict['AccountInfo'] = temp3

    # Fundid = row6
    # startingrow += 1
    # startingcol = 1
    # for c in cursor:
    #   startingcol += 1

    workbook2 = openpyxl.load_workbook(Masterpath)

    Mastersheet = workbook2.get_sheet_by_name('Main')
    Mastersheet.cell(row=16, column=3).value = input_dict['AnnuitantDOB']
    Mastersheet.cell(row=17, column=3).value = input_dict['BeneficiaryDOB']

    Mastersheet.cell(row=18, column=3).value = input_dict['LastfundedContributiondates'][1]
    Mastersheet.cell(row=18, column=3).number_format = 'mm/dd/yyyy'
    Mastersheet.cell(row=19, column=3).value = input_dict['LastfundedContributiondates'][2]
    Mastersheet.cell(row=19, column=3).number_format = 'mm/dd/yyyy'
    Mastersheet.cell(row=13, column=6).value = input_dict['Avg']
    Mastersheet.cell(row=15, column=6).value = input_dict['YMCA_401A']
    Mastersheet.cell(row=16, column=6).value = input_dict['PART_401A']
    Mastersheet.cell(row=17, column=6).value = input_dict['403b%']
    Mastersheet.cell(row=18, column=6).value = input_dict['Payroll_freq']

    noofRows = len(input_dict.get('AccountInfo').get('fundid'))
    initialRow = 29
    for printrow in range(0, noofRows):
        Mastersheet.cell(row=initialRow, column=2).value = input_dict.get('AccountInfo').get('fundid')[printrow]
        Mastersheet.cell(row=initialRow, column=3).value = input_dict.get('AccountInfo').get('accType')[printrow]
        Mastersheet.cell(row=initialRow, column=4).value = input_dict.get('AccountInfo').get('annBseType')[printrow]
        Mastersheet.cell(row=initialRow, column=5).value = input_dict.get('AccountInfo').get('perPreTax')[printrow]
        Mastersheet.cell(row=initialRow, column=6).value = input_dict.get('AccountInfo').get('perPosTax')[printrow]
        Mastersheet.cell(row=initialRow, column=7).value = input_dict.get('AccountInfo').get('ymcPreTax')[printrow]
        initialRow = initialRow + 1

    workbook2.save("C:\\Users\\selvam\\PycharmProjects\\UAC_TestTool_Demo.xlsx")
    Masterpath2 = "C:\\Users\\selvam\\PycharmProjects\\UAC_TestTool_Demo.xlsx"

    excel = win32.gencache.EnsureDispatch('Excel.Application')
    workbook = excel.Workbooks.Open('C:\\Users\\selvam\\PycharmProjects\\UAC_TestTool_Demo.xlsx')
    # this must be the absolute path (r'C:/abc/def/ghi')
    workbook.Save()
    workbook.Close()
    excel.Quit()
    workbook3 = openpyxl.load_workbook(Masterpath2, data_only=True)
    Mastersheet2 = workbook3.get_sheet_by_name('Main')

    Excel_dict = {}

    outputsheetfrml.cell(row=r + 1, column=2).value = Mastersheet2['O12'].internal_value
    outputsheetfrml.cell(row=r + 1, column=3).value = Mastersheet2['O13'].internal_value
    outputsheetfrml.cell(row=r + 1, column=4).value = Mastersheet2['O14'].internal_value
    outputsheetfrml.cell(row=r + 1, column=5).value = Mastersheet2['O15'].internal_value
    outputsheetfrml.cell(row=r + 1, column=6).value = Mastersheet2['O16'].internal_value
    outputsheetfrml.cell(row=r + 1, column=7).value = Mastersheet2['O17'].internal_value
    outputsheetfrml.cell(row=r + 1, column=8).value = Mastersheet2['O18'].internal_value
    outputsheetfrml.cell(row=r + 1, column=9).value = Mastersheet2['O19'].internal_value

    Excel_dict[inputfundId] = {'M':outputsheetfrml.cell(row=r + 1, column=2).value,'C':outputsheetfrml.cell(row=r + 1, column=3).value
                               ,'J1':outputsheetfrml.cell(row=r + 1, column=4).value,
                               'J1P':outputsheetfrml.cell(row=r + 1, column=5).value,
                               'J7':outputsheetfrml.cell(row=r + 1, column=6).value,
                               'J7P':outputsheetfrml.cell(row=r + 1, column=7).value,
                               'J5':outputsheetfrml.cell(row=r + 1, column=8).value,
                               'J5P':outputsheetfrml.cell(row=r + 1, column=9).value}
    print(Excel_dict[inputfundId])

inputbook.save("C:\\Users\\selvam\\PycharmProjects\\TextData.xlsx")