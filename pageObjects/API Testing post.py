import json

import requests

# Payload={
# "name": "palani",
# "job": "Automation tester"
# }
# json_response= requests.post("https://reqres.in/api/users",data= Payload, verify=False)
{

    "contributionAssumptions": {

        "stopWorkDate": "2023-01-01",

        "tdContributions": [

            {

                "amount": 5000,

                "kind": "LUMP_SUM",

                "periodicity": "PAY_PERIOD"

            }

        ],

        "assumedAnnualSalary": 115573.92

    },

    "retirementDate": "2023-07-01",

    "rdbSelection": 0,

    "plans": [

        "RETIREMENT",

        "SAVINGS"

    ],

    "survivor": {

        "dob": "1964-02-04",

        "name": "Kristy Meuter",

        "relationship": "SPOUSE"

    }

}
mydata = open("data.json", "r").read()
json_response = requests.post("https://reqres.in/api/users", data=json.loads(mydata), verify=False)
print(json_response.json())
assert json_response.json()['job'] == 'Automation tester', 'value not matching'
@step('Setup Request Body', suppress_arguments=True)
def setup_request_body(retirement_date=None):
    body = {
        "retirement_date": retirement_date.strftime('%Y-%m-%d'),
        "contribution_assumptions": {
            "stopWorkDate": stop_work_date.strftime('%Y-%m-%d'),
            "assumedAnnualSalary": contribution_annual_salary,
            "tdContributions": contributions
        },
        "account_plan": account_plan,
        "annual_salary_percent_increase": annual_salary_percent_increase,
        "rdb_selection": rdb_selection
    }

    if has_survivor:
        survivor_name = data['survivor_name']
        survivor_relationship = data['survivor_relationship']
        survivor_dob = datetime.strptime(data['survivor_dob'], '%Y-%m-%d')
        body['survivor'] = {
            "name": survivor_name,
            "relationship": survivor_relationship,
            "dob": survivor_dob.strftime('%Y-%m-%d')
        }
    report.info(f'Request Body:').code(body)
    return body

@step('Call the Estimates Calculator API', suppress_arguments=True)
def call_api(body: Dict):

    url = f'{environment["estimates_calculator_api_base_url"]}/api/annuity/estimate/normal/{fund_id}'
    report.info(f'Url: {url}')
    response = requests.post(url, json=body)

    report.info(f'API Call Status Code: {response.status_code}')
    report.info(f'API Call Response Text:')
    report.code(response.text)

    hassert(response.status_code).named('API Call Response Code').equal(200)

    return response

body = setup_request_body()
response = call_api(body)
