from selenium import webdriver
from selenium.webdriver import Keys
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
import openpyxl
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import time

#from pageObjects.databaseconnection import Excel_dict, inputfundId


class LoginPage():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.maximize_window()
    driver.get("http://nycwebdev02.ymcaret.org/YRS-INTEGRATED-QA-TEST-2/Login.aspx")
    time.sleep(5)
    driver.find_element(By.ID, 'TextBoxUserID').send_keys("selvam")
    driver.find_element(By.ID, 'TextBoxPassword').send_keys("Welcome1234!")
    driver.find_element(By.ID, 'ctl00_MainContent_OKButton').click()
    driver.find_element(By.XPATH, "//a[@id='lnkPageName'][contains (text(),'Estimates')]").click()
    path = "C:\\Users\\selvam\\PycharmProjects\\TextData.xlsx"
    inputbook1 = openpyxl.load_workbook(path)
    sheet1 = inputbook1.get_sheet_by_name('TextData')
    urlvalue = inputbook1.create_sheet("urlvalues")
    TestResult = inputbook1.create_sheet("Testresults")

    urlvalue.cell(row=1, column=1).value = 'Fund Id'
    urlvalue.cell(row=1, column=2).value = 'M'
    urlvalue.cell(row=1, column=3).value = 'C'
    urlvalue.cell(row=1, column=4).value = 'J1'
    urlvalue.cell(row=1, column=5).value = 'J1P'
    urlvalue.cell(row=1, column=6).value = 'J7'
    urlvalue.cell(row=1, column=7).value = 'J7P'
    urlvalue.cell(row=1, column=8).value = 'J5'
    urlvalue.cell(row=1, column=9).value = 'J5P'

    TestResult.cell(row=1, column=1).value = 'Fund Id'
    TestResult.cell(row=1, column=2).value = 'M'
    TestResult.cell(row=1, column=3).value = 'C'
    TestResult.cell(row=1, column=4).value = 'J1'
    TestResult.cell(row=1, column=5).value = 'J1P'
    TestResult.cell(row=1, column=6).value = 'J7'
    TestResult.cell(row=1, column=7).value = 'J7P'
    TestResult.cell(row=1, column=8).value = 'J5'
    TestResult.cell(row=1, column=9).value = 'J5P'

    rows = sheet1.max_row

    for r in range(1, rows + 1):
        driver.get("http://nycwebdev02.ymcaret.org/YRS-INTEGRATED-QA-TEST-2/FindInfo.aspx?Name=Estimates")
        inputfundId1 = str(sheet1.cell(row=r, column=1).value)
        urlvalue.cell(row=r + 1, column=1).value = sheet1.cell(row=r, column=1).value
        TestResult.cell(row=r + 1, column=1).value = sheet1.cell(row=r, column=1).value

        driver.find_element(By.ID, 'TextboxFundNo').send_keys(inputfundId1)
        driver.find_element(By.XPATH, "//input[@value='Find']").click()
        wait = WebDriverWait(driver, 50)
        element = wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@type='image']")))
        element.click()
        time.sleep(5)
        driver.find_element(By.XPATH, "//span[@id='PopcalendarDate_Control']").click()
        driver.find_element(By.XPATH, "//span[@id='popupSuperSpanYear0']").click()
        driver.find_element(By.XPATH, "//td[contains(text(),'2023')]").click()
        driver.find_element(By.XPATH, "//span[@id='popupSuperSpanMonth0']").click()
        driver.find_element(By.XPATH, "//td[contains(text(),'February')]").click()
        driver.find_element(By.XPATH,
                            "(//span[@id='popupSuperContent0']//table//span[contains(text(),'1')])[1]").click()
        driver.find_element(By.XPATH, "//input[@id='Buttoncalculate']").click()
        time.sleep(10)
        page = driver.find_element(By.TAG_NAME, ("html"))
        page.send_keys(Keys.END)
        time.sleep(10)

        summarytable = driver.find_element(By.XPATH, ("//table[@id='DatatGridSocialSecurityLevel']"))
        summaryrow = len(driver.find_elements(By.XPATH, ("//table[@id='DatatGridSocialSecurityLevel']//tr")))
        input_dict1 = {}

        for i in range(1, summaryrow + 1):
            Annuity = driver.find_element(By.XPATH,
                                          ("//table[@id='DatatGridSocialSecurityLevel']//tbody//tr[" + str(
                                              i) + "]/td[1]")).text
            Retire = driver.find_element(By.XPATH, (
                    "//table[@id='DatatGridSocialSecurityLevel']//tbody//tr[" + str(i) + "]/td[2]")).text
            Survivor = driver.find_element(By.XPATH, (
                    "//table[@id='DatatGridSocialSecurityLevel']//tbody//tr[" + str(i) + "]/td[3]")).text
            Beneficiary = driver.find_element(By.XPATH, (
                    "//table[@id='DatatGridSocialSecurityLevel']//tbody//tr[" + str(i) + "]/td[4]")).text
            input_dict1[Annuity] = Retire

        url_dict = {}

        urlvalue.cell(row=r + 1, column=2).value = input_dict1.get('M', '')
        urlvalue.cell(row=r + 1, column=3).value = input_dict1.get('C', '')
        urlvalue.cell(row=r + 1, column=4).value = input_dict1.get('J1', '')
        urlvalue.cell(row=r + 1, column=5).value = input_dict1.get('J1P', '')
        urlvalue.cell(row=r + 1, column=6).value = input_dict1.get('J7', '')
        urlvalue.cell(row=r + 1, column=7).value = input_dict1.get('J7P', '')
        urlvalue.cell(row=r + 1, column=8).value = input_dict1.get('J5', '')
        urlvalue.cell(row=r + 1, column=9).value = input_dict1.get('J5P', '')

        url_dict[inputfundId1] = {'M': urlvalue.cell(row=r + 1, column=2).value,
                                  'C': urlvalue.cell(row=r + 1, column=3).value, 'J1': urlvalue.cell(row=r + 1, column=4).value,
                                  'J1P': urlvalue.cell(row=r + 1, column=5).value,
                                  'J7': urlvalue.cell(row=r + 1, column=6).value,
                                  'J7P': urlvalue.cell(row=r + 1, column=7).value,
                                  'J5': urlvalue.cell(row=r + 1, column=8).value,
                                  'J5P': urlvalue.cell(row=r + 1, column=9).value}

        print(url_dict[inputfundId1])

    inputbook1.save("C:\\Users\\selvam\\PycharmProjects\\TextData.xlsx")
